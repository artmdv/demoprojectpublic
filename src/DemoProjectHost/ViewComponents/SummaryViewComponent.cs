﻿using System.Threading.Tasks;
using DemoProject.Domain.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DemoProject.UI.Host.ViewComponents
{
	public class SummaryViewComponent : ViewComponent
	{
		private readonly DomainNotificationHandler _notifications;

		public SummaryViewComponent(INotificationHandler<DomainNotification> notifications)
		{
			_notifications = (DomainNotificationHandler)notifications;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var notifications = await Task.FromResult((_notifications.GetNotifications()));
			foreach (var domainNotification in notifications)
			{
				ViewData.ModelState.AddModelError(string.Empty, domainNotification.Value);
			}

			return View();
		}
	}
}
