﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DemoProject.Application.Contracts;
using DemoProject.Application.Services;
using DemoProject.Domain.Notifications;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoProject.UI.Host.Controllers
{
    public class ProductsController: Controller
    {
        private readonly IProductService _productService;
	    private readonly DomainNotificationHandler _notifications;

	    public ProductsController(IProductService productService, INotificationHandler<DomainNotification> notifications)
	    {
		    _productService = productService;
		    _notifications = (DomainNotificationHandler)notifications;
	    }

        [HttpGet]
        [Route("products")]
        public async Task<IActionResult> Index()
        {
            return View(await _productService.GetAllAsync());
        }
		
		[HttpGet]
        [Route("products/export")]
        public async Task<IActionResult> Export()
        {
	        var exportedFile = await _productService.ExportAsync();

	        if (exportedFile == null || !exportedFile.Any())
	        {
		        return NotFound();
	        }
	        Stream exportStream = new MemoryStream(exportedFile);
	        return File(exportStream, "application/octet-stream", "products.xlsx");
        }
		
		[HttpPost]
        [Route("products/search")]
        public async Task<IActionResult> Search(string query)
        {
            return View("Index", await _productService.SearchAsync(query));
        }

        [HttpGet]
        [Route("products/{id:guid}/photo")]
        public async Task<IActionResult> Photo(Guid? id)
        {
	        if (id == null)
	        {
		        return NotFound();
	        }

            var photoBytes = await _productService.GetPhotoAsync(id.Value);

            if (photoBytes == null || !photoBytes.Any())
            {
                return NotFound();
            }
            Stream photoStream = new MemoryStream(photoBytes);
            return File(photoStream, "application/octet-stream");
        }
        
        [HttpGet]
        [Route("products/add")]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [Route("products/add")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(Product product, IFormFile photo)
        {
	        if (!ModelState.IsValid)
	        {
		        return View(product);
	        }

	        //could be done via file streaming if we want to work with big files (above ~28Mb). But takes more time 
	        if (photo != null)
	        {
		        using (var ms = new MemoryStream())
		        {
			        photo.CopyTo(ms);
			        var fileBytes = ms.ToArray();
			        product.Photo = fileBytes;
		        }
	        }
			
            await _productService.AddAsync(product);

	        if (!_notifications.HasNotifications())
	        {
		        TempData["Success"] = $"Product '{product.Name}' added";
	        }

	        return RedirectToAction("Index");
        }
		
	    [HttpGet]
	    [Route("products/{id:guid}/details")]
	    public async Task<IActionResult> Details(Guid? id)
	    {
		    if (id == null)
		    {
			    return NotFound();
		    }

		    var product = await _productService.GetByIdAsync(id.Value);

		    if (product == null)
		    {
			    return NotFound();
		    }

		    return View(product);
	    }
		
		[HttpGet]
	    [Route("products/{id:guid}/edit")]
	    public async Task<IActionResult> Edit(Guid? id)
	    {
		    if (id == null)
		    {
			    return NotFound();
		    }

		    var product = await _productService.GetByIdAsync(id.Value);

		    if (product == null)
		    {
			    return NotFound();
		    }

		    return View(product);
	    }

	    [HttpPost]
	    [Route("products/{id:guid}/edit")]
	    [ValidateAntiForgeryToken]
	    public async Task<IActionResult> Edit(Product product, IFormFile photo)
	    {
		    if (!ModelState.IsValid)
		    {
			    return View(await _productService.GetByIdAsync(product.Id));
		    }

		    //could be done via file streaming if we want to work with big files (above ~28Mb). But takes more time 
		    if (photo != null)
		    {
			    using (var ms = new MemoryStream())
			    {
				    photo.CopyTo(ms);
				    var fileBytes = ms.ToArray();
				    product.Photo = fileBytes;
			    }
		    }

		    await _productService.EditAsync(product);

		    if (!_notifications.HasNotifications())
		    {
			    TempData["Success"] = "Product successfully edited.";
		    }

		    return View(await _productService.GetByIdAsync(product.Id));
	    }

	    [HttpGet]
	    [Route("products/{id:guid}/remove")]
	    public async Task<IActionResult> Delete(Guid? id)
	    {
		    if (id == null)
		    {
			    return NotFound();
		    }

		    var product = await _productService.GetByIdAsync(id.Value);

		    if (product == null)
		    {
			    return NotFound();
		    }

		    return View(product);
	    }

	    [HttpPost, ActionName("Delete")]
	    [Route("products/{id:guid}/remove")]
	    [ValidateAntiForgeryToken]
	    public async Task<IActionResult> RemoveConfirmed(Guid id)
	    {
		    await _productService.RemoveAsync(id);

		    if (_notifications.HasNotifications())
		    {
			    return View(await _productService.GetByIdAsync(id));
		    }

		    TempData["Success"] = "Product removed.";
		    return RedirectToAction("Index");
	    }
    }
}
