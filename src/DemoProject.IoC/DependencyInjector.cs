﻿using DemoProject.Application.Services;
using DemoProject.Domain.CommandHandlers;
using DemoProject.Domain.Commands;
using DemoProject.Domain.Interfaces;
using DemoProject.Domain.Notifications;
using DemoProject.Infrastructure;
using DemoProject.Infrastructure.Repositories;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace DemoProject.Configuration
{
    public class DependencyInjector
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IProductService, ProductService>();
	        services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
	        services.AddScoped<IRequestHandler<AddProductCommand, Unit>, AddProductCommandHandler>();
	        services.AddScoped<IRequestHandler<RemoveProductCommand, Unit>, RemoveProductCommandHandler>();
	        services.AddScoped<IRequestHandler<EditProductCommand, Unit>, EditProductCommandHandler>();
	        services.AddScoped<IProductRepository, ProductRepository>();
	        services.AddScoped<ProductContext>();

        }
    }

}
