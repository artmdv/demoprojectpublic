﻿using System;
using DemoProject.Domain.Validations;

namespace DemoProject.Domain.Commands
{
    public class EditProductCommand: ProductCommand
    {
        public EditProductCommand(Guid id, string code, string name, decimal price, byte[] photo)
        {
            Id = id;
            Code = code;
            Name = name;
            Price = price;
	        Photo = photo;
        }

        public override bool IsValid()
        {
            ValidationResult = new EditProductCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
