﻿using DemoProject.Domain.Validations;

namespace DemoProject.Domain.Commands
{
    public class AddProductCommand: ProductCommand
    {
        public AddProductCommand(string code, string name, decimal price, byte[] photo)
        {
            Code = code;
            Name = name;
            Price = price;
	        Photo = photo;
        }

        public override bool IsValid()
        {
            ValidationResult = new AddProductCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
