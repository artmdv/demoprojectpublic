﻿using System;
using MediatR;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace DemoProject.Domain.Commands
{
    public abstract class Command : IRequest<Unit>
    {
        public DateTime Timestamp { get; private set; }
        public ValidationResult ValidationResult { get; set; }
        public Guid Id { get; set; }

        protected Command()
        {
            Timestamp = DateTime.Now;
        }

        public abstract bool IsValid();
    }
}
