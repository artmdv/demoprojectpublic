﻿namespace DemoProject.Domain.Commands
{
    public abstract class ProductCommand: Command
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
		public byte[] Photo { get; set; }
    }
}
