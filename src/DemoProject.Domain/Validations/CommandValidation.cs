﻿using System;
using DemoProject.Domain.Commands;
using FluentValidation;

namespace DemoProject.Domain.Validations
{
    public class CommandValidation<T>: AbstractValidator<T> where T: Command
    {
        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEmpty()
                .NotEqual(default(Guid))
                .NotEqual(Guid.Empty);
        }
    }
}
