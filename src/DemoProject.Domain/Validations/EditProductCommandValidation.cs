﻿using DemoProject.Domain.Commands;

namespace DemoProject.Domain.Validations
{

    public class EditProductCommandValidation : ProductValidation<EditProductCommand>
    {
        public EditProductCommandValidation()
        {
            ValidateId();
            ValidateCode();
            ValidateName();
            ValidatePrice();
        }
    }
}
