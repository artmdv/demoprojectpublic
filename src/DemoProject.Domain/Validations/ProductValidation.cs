﻿using DemoProject.Domain.Commands;
using FluentValidation;

namespace DemoProject.Domain.Validations
{
    public class ProductValidation<T>: CommandValidation<T> where T: ProductCommand
    {
        protected void ValidateCode()
        {
            RuleFor(c => c.Code)
                .NotEmpty().WithMessage("Code must be provided");
        }

        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Name must be provided");
        }

        protected void ValidatePrice()
        {
            RuleFor(c => c.Price)
                .NotEmpty().WithMessage("Price must be provided")
                .GreaterThan(0).WithMessage("Price must be greater than 0.00");
        }
    }
}
