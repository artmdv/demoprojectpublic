﻿using DemoProject.Domain.Commands;

namespace DemoProject.Domain.Validations
{
    public class AddProductCommandValidation: ProductValidation<AddProductCommand>
    {
        public AddProductCommandValidation()
        {
            ValidateCode();
            ValidateName();
            ValidatePrice();
        }
    }
}
