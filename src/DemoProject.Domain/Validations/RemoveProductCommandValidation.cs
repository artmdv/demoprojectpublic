﻿using DemoProject.Domain.Commands;

namespace DemoProject.Domain.Validations
{
    public class RemoveProductCommandValidation: CommandValidation<RemoveProductCommand>
    {
        public RemoveProductCommandValidation()
        {
            ValidateId();
        }
    }
}
