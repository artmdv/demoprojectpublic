﻿using System;
using MediatR;

namespace DemoProject.Domain.Notifications
{
	public class DomainNotification : INotification
	{
		public Guid DomainNotificationId { get; private set; }
		public string Value { get; private set; }

		public DomainNotification(string value)
		{
			DomainNotificationId = Guid.NewGuid();
			Value = value;
		}
	}
}
