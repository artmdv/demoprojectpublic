﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace DemoProject.Domain.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        Task AddAsync(TEntity obj);
        Task<TEntity> GetByIdAsync(Guid id);
        Task<IQueryable<TEntity>> GetAllAsync();
        Task UpdateAsync(TEntity obj);
	    Task RemoveAsync(Guid id);
        Task<int> SaveChangesAsync();
    }
}
