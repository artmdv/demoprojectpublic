﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DemoProject.Domain.Models;

namespace DemoProject.Domain.Interfaces
{
	public interface IProductRepository: IRepository<Product>
	{
		Task SavePhotoAsync(Guid photoId, byte[] requestPhoto);
		Task<byte[]> GetPhotoAsync(Guid photoId);
		Task<IQueryable<Product>> FindAsync(string query);
		Task<Product> GetByCodeAsync(string code);
		Task<byte[]> Export();
	}
}
