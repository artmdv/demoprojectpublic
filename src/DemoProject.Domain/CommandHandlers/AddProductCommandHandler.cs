﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DemoProject.Domain.Commands;
using DemoProject.Domain.Interfaces;
using DemoProject.Domain.Models;
using DemoProject.Domain.Notifications;
using MediatR;

namespace DemoProject.Domain.CommandHandlers
{
	public class AddProductCommandHandler: CommandHandler, IRequestHandler<AddProductCommand>
	{
		private readonly IProductRepository _productRepository;

		public AddProductCommandHandler(IProductRepository productRepository, IMediator mediator): base(mediator)
		{
			_productRepository = productRepository;
		}


		public void Dispose()
		{
			_productRepository.Dispose();
		}

		public async Task<Unit> Handle(AddProductCommand request, CancellationToken cancellationToken)
		{
			if (!request.IsValid())
			{
				PublishValidationErrors(request);
				return Unit.Value;
			}

			Guid? photoId = null;
			if (request.Photo != null && request.Photo.Any())
			{
				photoId = Guid.NewGuid();
				await _productRepository.SavePhotoAsync(photoId.Value, request.Photo);
			}

			var product = new Product(Guid.NewGuid(), request.Code, request.Name, request.Price, photoId);

			if (await _productRepository.GetByCodeAsync(product.Code) != null)
			{
				Mediator.Publish(new DomainNotification("Product with such code already exists."));
				return Unit.Value;
			}

			await _productRepository.AddAsync(product);
			return Unit.Value;
		}
	}
}
