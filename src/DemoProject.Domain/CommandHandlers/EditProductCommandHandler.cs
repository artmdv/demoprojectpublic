﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DemoProject.Domain.Commands;
using DemoProject.Domain.Interfaces;
using DemoProject.Domain.Models;
using DemoProject.Domain.Notifications;
using MediatR;

namespace DemoProject.Domain.CommandHandlers
{
	public class EditProductCommandHandler: CommandHandler, IRequestHandler<EditProductCommand>
	{
		private readonly IProductRepository _productRepository;
		private readonly IMediator _mediator;

		public EditProductCommandHandler(IProductRepository productRepository, IMediator mediator): base(mediator)
		{
			_productRepository = productRepository;
			_mediator = mediator;
		}


		public void Dispose()
		{
			_productRepository.Dispose();
		}

		public async Task<Unit> Handle(EditProductCommand request, CancellationToken cancellationToken)
		{
			if (!request.IsValid())
			{
				PublishValidationErrors(request);
				return Unit.Value;
			}

			var existingProduct = await _productRepository.GetByIdAsync(request.Id);
			
			var photoId = existingProduct.PhotoId;

			if (request.Photo != null && request.Photo.Any())
			{
				photoId = Guid.NewGuid();
				await _productRepository.SavePhotoAsync(photoId.Value, request.Photo);
			}

			var product = new Product(request.Id, request.Code, request.Name, request.Price, photoId);

			var productWithSameCode = await _productRepository.GetByCodeAsync(product.Code);

			if (productWithSameCode != null && !productWithSameCode.Equals(existingProduct))
			{
				_mediator.Publish(new DomainNotification("Product with such code already exists."));
				return Unit.Value;
			}

			await _productRepository.UpdateAsync(product);
			return Unit.Value;
		}
	}
}
