﻿using System.Threading;
using System.Threading.Tasks;
using DemoProject.Domain.Commands;
using DemoProject.Domain.Interfaces;
using MediatR;

namespace DemoProject.Domain.CommandHandlers
{
	public class RemoveProductCommandHandler: CommandHandler, IRequestHandler<RemoveProductCommand>
	{
		private readonly IProductRepository _productRepository;

		public RemoveProductCommandHandler(IProductRepository productRepository, IMediator mediator): base(mediator)
		{
			_productRepository = productRepository;
		}

		public void Dispose()
		{
			_productRepository.Dispose();
		}

		public async Task<Unit> Handle(RemoveProductCommand request, CancellationToken cancellationToken)
		{
			if (!request.IsValid())
			{
				PublishValidationErrors(request);
				return Unit.Value;
			}

			await _productRepository.RemoveAsync(request.Id);

			return Unit.Value;
		}
	}
}
