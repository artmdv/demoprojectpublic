﻿using DemoProject.Domain.Commands;
using DemoProject.Domain.Notifications;
using MediatR;

namespace DemoProject.Domain.CommandHandlers
{
	public abstract class CommandHandler
	{
		protected readonly IMediator Mediator;

		protected CommandHandler(IMediator mediator)
		{
			Mediator = mediator;
		}

		protected void PublishValidationErrors(Command request)
		{
			foreach (var error in request.ValidationResult.Errors)
			{
				Mediator.Publish(new DomainNotification(error.ErrorMessage));
			}
		}
	}
}
