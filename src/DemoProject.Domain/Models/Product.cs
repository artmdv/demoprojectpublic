﻿using System;

namespace DemoProject.Domain.Models
{
	public class Product: Entity
	{
		public Product(Guid id, string code, string name, decimal price, Guid? photoId=null)
		{
			Id = id;
			Code = code;
			Name = name;
			Price = price;
			PhotoId = photoId;
			LastUpdated = DateTime.Now;
		}

		public string Code { get; set; }
		public string Name { get; set; }
		public Guid? PhotoId { get; set; }
		public decimal Price { get; set; }
		public DateTime LastUpdated { get; set; }
	}
}
