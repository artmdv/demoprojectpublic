﻿using System.Threading;
using System.Threading.Tasks;
using DemoProject.Domain.CommandHandlers;
using DemoProject.Domain.Commands;
using DemoProject.Domain.Interfaces;
using DemoProject.Domain.Models;
using DemoProject.Domain.Notifications;
using MediatR;
using Moq;
using NUnit.Framework;

namespace DemoProject.Tests
{
	[TestFixture]
	public class CommandHandlerTests
	{
		private Mock<IProductRepository> _productRepository;
		private Mock<IMediator> _mediator;

		[SetUp]
		public void Setup()
		{
			_productRepository = new Mock<IProductRepository>();
			_mediator = new Mock<IMediator>();
		}

		[Test]
		public async Task GivenAddProductCommandIsMissingNonDefaultValuesDomainNotificationIsPublishedAndProductNotStoredInRepository()
		{
			var command = new AddProductCommand(null, null, default(decimal), null);
			var handler = new AddProductCommandHandler(_productRepository.Object, _mediator.Object);

			await handler.Handle(command, CancellationToken.None);

			_mediator.Verify(x=>x.Publish(It.Is<DomainNotification>(notification => notification.Value == "Code must be provided"), It.IsAny<CancellationToken>()), Times.Once);
			_mediator.Verify(x=>x.Publish(It.Is<DomainNotification>(notification => notification.Value == "Name must be provided"), It.IsAny<CancellationToken>()), Times.Once);
			_mediator.Verify(x=>x.Publish(It.Is<DomainNotification>(notification => notification.Value == "Price must be provided"), It.IsAny<CancellationToken>()), Times.Once);
			_productRepository.Verify(x=>x.AddAsync(It.IsAny<Product>()), Times.Never);
		}
		
		[TestCase(-1)]
		[TestCase(-0.001)]
		[TestCase(-100)]
		[TestCase(-4654)]
		[TestCase(0)]
		public async Task GivenAddProductCommandPriceMyBeAboveZeroDomainNotificationIsPublishedAndProductNotStoredInRepository(decimal price)
		{
			var command = new AddProductCommand("a", "a", price, null);
			var handler = new AddProductCommandHandler(_productRepository.Object, _mediator.Object);

			await handler.Handle(command, CancellationToken.None);

			_mediator.Verify(x=>x.Publish(It.Is<DomainNotification>(notification => notification.Value == "Price must be greater than 0.00"), It.IsAny<CancellationToken>()), Times.Once);
			_productRepository.Verify(x=>x.AddAsync(It.IsAny<Product>()), Times.Never);
		}

		//TODO: verify the behaviour of command handlers
		//TODO: check validations
	}
}
