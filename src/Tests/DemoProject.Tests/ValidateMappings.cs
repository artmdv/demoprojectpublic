﻿using DemoProject.Application.Configuration;
using NUnit.Framework;

namespace DemoProject.Tests
{
	[TestFixture]
	public class ValidateMappings
	{
		[Test]
		public void ValidateProfileMappings()
		{
			AutoMapperConfig.RegisterMappings().AssertConfigurationIsValid();
		}

	}
}
