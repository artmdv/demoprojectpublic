﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DemoProject.Application.Contracts;
using Newtonsoft.Json;
using NUnit.Framework;
using Product = DemoProject.API.Host.Contracts.Product;

namespace DemoProject.Tests
{
	[TestFixture(Explicit = true)]
	public class ApiIntegrationTests
	{
		//TODO: move to config
		private const string Address = "https://localhost:5001/api/v1/Products/";

		//TODO: tests for each endpoint. 
		//Add product, try getting with search to see if it actually created, then remove it
		//Edit product, check if updated (precondition- create, postcondition- delete)
		//Get- create multiple products, check if all returned in list. delete all
		//Delete- create, delete, verify no longer found
		//get with query (precondition- create, postcondition- delete)
		//export works and expected entries found (precondition- create, postcondition- delete)
		//assign image, get image, verify same (precondition- create, postcondition- delete)
		//update image, get image, verify updated (precondition- create, postcondition- delete)

		[Test]
		public async Task GivenNonExistingIdIsProvidedWhenCallingGetByIdApiReturnsNotFound()
		{
			var guid = Guid.NewGuid();
			var restClient = new HttpClient {BaseAddress = new Uri(Address)};
			var result = await restClient.GetAsync(guid.ToString());
			Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
		}
		
		[Test]
		public async Task GivenNewProductIsAddedThenItIsReturnedByProductsList()
		{
			var restClient = new HttpClient {BaseAddress = new Uri(Address)};

			var product = new Product
			{
				Code = Guid.NewGuid().ToString(),
				Name = "name",
				Price = 1
			};

			await restClient.PostAsync("", new StringContent(JsonConvert.SerializeObject(product), Encoding.UTF8, "application/json"));
			var result = await restClient.GetAsync("");
			var listJsonResult = await result.Content.ReadAsStringAsync();
			var listResult = JsonConvert.DeserializeObject<IEnumerable<ProductReadmodel>>(listJsonResult);

			var addedProduct = listResult.Single(x => x.Code == product.Code);

			Assert.That(addedProduct, Is.Not.Null);

			var deleteResult = await restClient.DeleteAsync(addedProduct.Id.ToString());
			Assert.That(deleteResult.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));
		}
	}
}
