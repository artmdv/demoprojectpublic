﻿using Microsoft.EntityFrameworkCore;
using System.IO;
using DemoProject.Domain.Models;
using DemoProject.Infrastructure.Mappings;
using DemoProject.Infrastructure.Models;
using Microsoft.Extensions.Configuration;

namespace DemoProject.Infrastructure
{
	public class ProductContext : DbContext
	{
		public DbSet<Product> Products { get; set; }
		public DbSet<ProductPhoto> ProductPhotos { get; set; }

		public ProductContext(DbContextOptions<ProductContext> options)
			: base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfiguration(new ProductMap());
			modelBuilder.ApplyConfiguration(new ProductPhotoMap());
                        
			base.OnModelCreating(modelBuilder);
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			// get the configuration from the app settings
			var config = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json")
				.Build();
            
			// define the database to use
			optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
		}
	}
}
