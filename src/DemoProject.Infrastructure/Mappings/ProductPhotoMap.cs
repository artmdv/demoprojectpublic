﻿using DemoProject.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DemoProject.Infrastructure.Mappings
{
	class ProductPhotoMap: IEntityTypeConfiguration<ProductPhoto>
	{
		public void Configure(EntityTypeBuilder<ProductPhoto> builder)
		{
			builder.Property(c => c.Id)
				.HasColumnName("Id");

			builder.Property(c => c.Photo)
				.HasColumnType("image")
				.IsRequired();
		}
	}
}
