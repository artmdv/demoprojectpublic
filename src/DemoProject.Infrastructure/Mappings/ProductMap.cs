﻿using DemoProject.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DemoProject.Infrastructure.Mappings
{
	public class ProductMap : IEntityTypeConfiguration<Product>
	{
		public void Configure(EntityTypeBuilder<Product> builder)
		{
			builder.Property(c => c.Id)
				.HasColumnName("Id");

			builder.Property(c => c.Name)
				.HasColumnType("varchar(500)")
				.HasMaxLength(500)
				.IsRequired();
			
			builder.Property(c => c.Code)
				.HasColumnType("varchar(500)")
				.HasMaxLength(500)
				.IsRequired();
			
			builder.Property(c => c.Price)
				.HasColumnType("decimal(11,2)")
				.IsRequired();
			
			builder.Property(c => c.LastUpdated)
				.HasColumnType("datetime2(7)")
				.IsRequired();

			builder.Property(c => c.PhotoId)
				.HasColumnType("varchar(36)")
				.HasMaxLength(36);

			builder.HasIndex(c => c.Code).IsUnique();
		}
	}
}
