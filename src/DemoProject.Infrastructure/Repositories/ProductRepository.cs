﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DemoProject.Domain.Interfaces;
using DemoProject.Domain.Models;
using DemoProject.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using NPOI.XSSF.UserModel;

namespace DemoProject.Infrastructure.Repositories
{
	public class ProductRepository: Repository<Product>, IProductRepository
	{
		public ProductRepository(ProductContext context) : base(context)
		{
		}

		public async Task SavePhotoAsync(Guid photoId, byte[] requestPhoto)
		{
			await Db.ProductPhotos.AddAsync(new ProductPhoto(photoId, requestPhoto));
			await Db.SaveChangesAsync();
		}

		public async Task<byte[]> GetPhotoAsync(Guid photoId)
		{
			return await Task.FromResult(Db.ProductPhotos.SingleOrDefault(x => x.Id == photoId)?.Photo);
		}

		public Task<IQueryable<Product>> FindAsync(string query)
		{
			return Task.FromResult(DbSet.Where(product => product.Code.Contains(query) || product.Name.Contains(query)));
		}

		public Task<Product> GetByCodeAsync(string code)
		{
			return Task.FromResult(DbSet.AsNoTracking().FirstOrDefault(c => c.Code == code));
		}

		public async Task<byte[]> Export()
		{
			var workbook = new XSSFWorkbook();
			var sheet = workbook.CreateSheet("products");
			var headers = new List<string>
			{
				nameof(Product.Id),
				nameof(Product.Code),
				nameof(Product.Name),
				nameof(Product.Price),
				nameof(Product.LastUpdated)
			};

			var row = 1;
			foreach (var product in DbSet)
			{
				var sheetRow = sheet.CreateRow(row++);
				sheetRow.CreateCell(0).SetCellValue(product.Id.ToString());
				sheetRow.CreateCell(1).SetCellValue(product.Code);
				sheetRow.CreateCell(2).SetCellValue(product.Name);
				sheetRow.CreateCell(3).SetCellValue(product.Price.ToString("N"));
				sheetRow.CreateCell(4).SetCellValue(product.LastUpdated.ToString("O"));
			}
			
			var header = sheet.CreateRow(0);
			for (var i = 0; i < headers.Count; i++)
			{
				var cell = header.CreateCell(i);
				cell.SetCellValue(headers[i]);
			}

			byte[] file; 
			using (var memoryStream = new MemoryStream()) 
			{
				workbook.Write(memoryStream);
				file = memoryStream.ToArray();
			}
			return await Task.FromResult(file);
		}
	}
}
