﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DemoProject.Domain.Interfaces;
using DemoProject.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.Infrastructure.Repositories
{
	public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
	{
		protected readonly ProductContext Db;
		protected readonly DbSet<TEntity> DbSet;

		public Repository(ProductContext context)
		{
			Db = context;
			DbSet = Db.Set<TEntity>();
		}

		public virtual async Task AddAsync(TEntity obj)
		{
			await DbSet.AddAsync(obj);
			await SaveChangesAsync();
		}

		public virtual async Task<TEntity> GetByIdAsync(Guid id)
		{
			return await Task.FromResult(DbSet.AsNoTracking().SingleOrDefault(x => x.Id == id));
		}

		public virtual async Task<IQueryable<TEntity>> GetAllAsync()
		{
			return await Task.FromResult(DbSet);
		}
		
		public virtual async Task UpdateAsync(TEntity obj)
		{
			DbSet.Update(obj);
			await SaveChangesAsync();
		}

		public virtual async Task RemoveAsync(Guid id)
		{
			DbSet.Remove(await DbSet.FindAsync(id));
			await SaveChangesAsync();
		}

		public async Task<int> SaveChangesAsync()
		{
			return await Db.SaveChangesAsync();
		}

		public void Dispose()
		{
			Db.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}
