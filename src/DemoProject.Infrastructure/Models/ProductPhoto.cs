﻿using System;
using DemoProject.Domain.Models;

namespace DemoProject.Infrastructure.Models
{
	public class ProductPhoto: Entity
	{
		public ProductPhoto(Guid id, byte[] photo)
		{
			Id = id;
			Photo = photo;
		}

		public byte[] Photo { get; set; }
	}
}
