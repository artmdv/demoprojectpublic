﻿using System.IO;
using System.Reflection;
using DemoProject.API.Host.Extensions;
using DemoProject.Configuration;
using DemoProject.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace DemoProject.API.Host
{
	public class Startup
	{
		public Startup(IHostingEnvironment env)
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
				.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);


			builder.AddEnvironmentVariables();
			Configuration = builder.Build();
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<ProductContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Transient);

			

			services.AddMvcCore()
				.AddJsonFormatters()
				.AddApiExplorer()
				.AddCors()
				.AddVersionedApiExplorer(
				options =>
				{
					options.GroupNameFormat = "'v'VVV";
					options.SubstituteApiVersionInUrl = true;
					options.AssumeDefaultVersionWhenUnspecified = true;
					options.DefaultApiVersion = new ApiVersion(1, 0);
				} );

			services.AddApiVersioning(options =>
			{
				options.ReportApiVersions = true;
				options.AssumeDefaultVersionWhenUnspecified = true;
				options.DefaultApiVersion = new ApiVersion(1,0);
			} );

			services.AddAutoMapperSetup();

			services.AddSwaggerGen(options =>
			{
				using ( var serviceProvider = services.BuildServiceProvider() )
				{
					var provider = serviceProvider.GetRequiredService<IApiVersionDescriptionProvider>();

					foreach ( var description in provider.ApiVersionDescriptions )
					{
						options.SwaggerDoc( description.GroupName, CreateInfoForApiVersion( description ) );
					}
				}
				options.IncludeXmlComments( XmlCommentsFilePath );
				options.OperationFilter<SwaggerFileOperation>();
			});

			services.AddMediatR(typeof(Startup));

			DependencyInjector.RegisterServices(services);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApiVersionDescriptionProvider provider)
		{
			loggerFactory.AddConsole();
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.UseCors(c =>
			{
				c.AllowAnyHeader();
				c.AllowAnyMethod();
				c.AllowAnyOrigin();
			});

			app.UseStaticFiles();

			app.UseHttpsRedirection();
			app.UseMvc();

			app.UseSwagger();
			app.UseSwaggerUI(options =>
			{
				// build a swagger endpoint for each discovered API version
				foreach ( var description in provider.ApiVersionDescriptions )
				{
					options.SwaggerEndpoint( $"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant() );
				}
			} );
		}

		static string XmlCommentsFilePath
		{
			get
			{
				var basePath = PlatformServices.Default.Application.ApplicationBasePath;
				var fileName = typeof( Startup ).GetTypeInfo().Assembly.GetName().Name + ".xml";
				return Path.Combine( basePath, fileName );
			}
		}

		static Info CreateInfoForApiVersion( ApiVersionDescription description )
		{
			var info = new Info
			{
				Title = $"DemoProject API {description.ApiVersion}",
				Version = description.ApiVersion.ToString(),
				Description = "DemoProject API showcasing my implementation of new project",
				Contact = new Contact { Name = "Artūras Medvedevas", Email = "arturas.medvedevas@gmail.com" },
			};

			return info;
		}
	}
}
