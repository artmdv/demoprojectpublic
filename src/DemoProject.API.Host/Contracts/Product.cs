﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DemoProject.API.Host.Contracts
{
	public class Product
	{
		[Required(ErrorMessage = "The Code is Required")]
		[MaxLength(100)]
		[DisplayName("Code")]
		public string Code { get; set; }

		[Required]
		[MaxLength(100)]
		[DisplayName("Product")]
		public string Name { get; set; }

		[Required]
		[Range(0, 999999999)]
		public decimal Price { get; set; }
	}
}
