﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DemoProject.Application.Contracts;
using DemoProject.Application.Services;
using DemoProject.Domain.Notifications;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoProject.API.Host.Controllers
{
	[ApiVersion( "1.0" )]
	[Route("api/v{version:apiVersion}/[controller]")]
	[ApiController]
	public class ProductsController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly IProductService _productService;
		private readonly DomainNotificationHandler _notifications;
		private List<string> Errors = new List<string>();

		public ProductsController(IMapper mapper,IProductService productService, INotificationHandler<DomainNotification> notifications)
		{
			_mapper = mapper;
			_productService = productService;
			_notifications = (DomainNotificationHandler)notifications;
		}

		/// <summary>
		/// Returns all available products
		/// </summary>
		/// <param name="query">Optional search string</param>
		/// <returns>Products</returns>
		/// <response code="200">Products retrieved successfully</response>
		[HttpGet]
		[ProducesResponseType( typeof( IEnumerable<ProductReadmodel> ), 200 )]
		public async Task<ActionResult<IEnumerable<ProductReadmodel>>> GetAll([FromQuery] string query)
		{
			IEnumerable<ProductReadmodel> list;
			if (string.IsNullOrWhiteSpace(query))
			{
				list = await _productService.GetAllAsync();
			}
			else
			{
				list = await _productService.SearchAsync(query);
			}
			if (HasErrors()) return BadRequestWithErrors();
			return Ok(list);
		}
		
		/// <summary>
		/// Returns product by Id
		/// </summary>
		/// <param name="id">Product Guid</param>
		/// <returns>Products</returns>
		/// <response code="200">Products retrieved successfully</response>
		[HttpGet("{id:Guid}")]
		[ProducesResponseType( typeof( ProductReadmodel ), 200 )]
		[ProducesResponseType( 404)]
		[ProducesResponseType( 400)]
		public async Task<ActionResult<ProductReadmodel>> Get([FromRoute] Guid id)
		{
			var product = await _productService.GetByIdAsync(id);

			if (product == null)
			{
				return NotFound();
			}
			
			if (HasErrors()) return BadRequestWithErrors();
			return Ok(product);
		}
		
		/// <summary>
		/// Returns product photo
		/// </summary>
		/// <param name="id">Product Guid</param>
		/// <returns>Product photo</returns>
		/// <response code="200">Photo returned successfully</response>
		/// <response code="400">Request contains errors</response>
		/// <response code="404">Product not found</response>
		[HttpGet("{id:Guid}/photo")]
		[ProducesResponseType( typeof( File ), 200 )]
		[ProducesResponseType( 400 )]
		[ProducesResponseType( 404 )]
		public async Task<ActionResult> Photo([FromRoute] Guid id)
		{
			var photoBytes = await _productService.GetPhotoAsync(id);

			if (HasErrors()) return BadRequestWithErrors();

			if (photoBytes == null || !photoBytes.Any())
			{
				return NotFound();
			}
			Stream photoStream = new MemoryStream(photoBytes);
			//TODO: save filename so we could return it with relevant extension
			return File(photoStream, "application/octet-stream", $"product_{id}.jpg");
		}
		
		/// <summary>
		/// Assigns photo to product 
		/// </summary>
		/// <param name="id">Product Guid</param>
		/// <param name="photo">Product photo</param>
		/// <response code="202">Photo returned successfully</response>
		/// <response code="400">Request contains errors</response>
		/// <response code="404">Product not found</response>
		[HttpPut("{id:Guid}/photo")]
		[ProducesResponseType( 202 )]
		[ProducesResponseType( 400 )]
		[ProducesResponseType( 404 )]
		public async Task<ActionResult> AssignPhoto([FromRoute] Guid id, [FromForm] List<IFormFile> photo)
		{
			var product = await _productService.GetByIdAsync(id);

			if (product == null)
			{
				return NotFound();
			}
			//TODO: smart thing here would be to have possibility to assign photo in service. Not enough time to do everything :(
			Product mappedProduct = _mapper.Map<Product>(product);

			if (photo != null && photo.Any())
			{
				using (var ms = new MemoryStream())
				{
					photo.First().CopyTo(ms);
					var fileBytes = ms.ToArray();
					mappedProduct.Photo = fileBytes;
				}
			}
			
			await _productService.EditAsync(mappedProduct);

			if (HasErrors()) return BadRequestWithErrors();

			return Accepted();
		}
		
		/// <summary>
		/// Returns product list in xlsx document
		/// </summary>
		/// <returns>Products in xlsx document</returns>
		/// <response code="200">Exported successfully</response>
		/// <response code="400">Request contains errors</response>
		[HttpGet("excel")]
		[ProducesResponseType( typeof( File ), 200 )]
		[ProducesResponseType( 400 )]
		public async Task<ActionResult> Export()
		{
			var exportedFile = await _productService.ExportAsync();

			if (HasErrors()) return BadRequestWithErrors();

			if (exportedFile == null || !exportedFile.Any())
			{
				return NotFound();
			}

			Stream exportStream = new MemoryStream(exportedFile);
			return File(exportStream, "application/octet-stream", "products.xlsx");
		}

		/// <summary>
		/// Add a new product
		/// </summary>
		/// <param name="product">Product</param>
		/// <returns>Added Product</returns>
		/// <response code="200">Products retrieved successfully</response>
		[HttpPost]
		[ProducesResponseType( 202 )]
		[ProducesResponseType( 400 )]
		public async Task<ActionResult> Add([FromBody]Contracts.Product product)
		{
			//TODO: make this service return the Added product. Update UI as well.
			await _productService.AddAsync(_mapper.Map<Product>(product));

			if (HasErrors()) return BadRequestWithErrors();
			return Accepted();
		}
		
		/// <summary>
		/// Update existing product
		/// </summary>
		/// <param name="id">Product Id</param>
		/// <param name="product">Product</param>
		/// <returns>Updated Product</returns>
		/// <response code="200">Product updated successfully</response>
		/// <response code="400">Request contains errors</response>
		/// <response code="404">Product not found</response>
		[HttpPost("{id:Guid}")]
		[ProducesResponseType( typeof( ProductReadmodel ), 200 )]
		[ProducesResponseType( 404)]
		[ProducesResponseType( 400)]
		public async Task<ActionResult<ProductReadmodel>> Update([FromRoute] Guid id, [FromBody]Contracts.Product product)
		{
			var existingProduct = await _productService.GetByIdAsync(id);
			if (existingProduct == null)
			{
				return NotFound();
			}

			var mappedProduct = _mapper.Map<Product>(product);

			mappedProduct.Id = id;

			await _productService.EditAsync(mappedProduct);

			if (HasErrors()) return BadRequestWithErrors();

			return Ok(product);
		}
		
		/// <summary>
		/// Deletes product
		/// </summary>
		/// <param name="id">Product Guid</param>
		/// <response code="204">Product deleted successfully</response>
		/// <response code="400">Request contains errors</response>
		/// <response code="404">Product not found</response>
		[HttpDelete("{id:Guid}")]
		[ProducesResponseType( 204 )]
		[ProducesResponseType( 400 )]
		[ProducesResponseType( 404 )]
		public async Task<ActionResult> Delete(Guid id)
		{
			//TODO: could be done in command handler validators. 
			//TODO: would need to introduce types to domain notifications probably, because doing this check in controller doesnt feel right and returning 400 when it's actually 404 doesn't either. 
			//TODO: needs more thought on this.
			var product = await _productService.GetByIdAsync(id);
			if (product == null)
			{
				return NotFound();
			}

			await _productService.RemoveAsync(id);
			
			if (HasErrors()) return BadRequestWithErrors();
			return NoContent();

		}
		private bool HasErrors()
		{
			Errors.AddRange(_notifications.GetNotifications().Select(x => x.Value));

			if (!ModelState.IsValid)
			{
				var erros = ModelState.Values.SelectMany(v => v.Errors);
				foreach (var erro in erros)
				{
					var erroMsg = erro.Exception == null ? erro.ErrorMessage : erro.Exception.Message;
					Errors.Add(erroMsg);
				}
			}

			return Errors.Any();
		}

		private ActionResult BadRequestWithErrors()
		{
			return BadRequest(new
			{
				Errors
			});
		}
	}
}
