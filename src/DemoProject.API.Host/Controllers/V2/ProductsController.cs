﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoProject.Application.Contracts;
using DemoProject.Application.Services;
using DemoProject.Domain.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DemoProject.API.Host.Controllers.V2
{
	[ApiVersion( "2.0" )]
	[Route("api/v{version:apiVersion}/[controller]")]
	[ApiController]
	public class ProductsController : ControllerBase
	{
		private readonly IProductService _productService;
		private readonly DomainNotificationHandler _notifications;

		public ProductsController(IProductService productService, INotificationHandler<DomainNotification> notifications)
		{
			_productService = productService;
			_notifications = (DomainNotificationHandler)notifications;
		}

		/// <summary>
		/// Returns all available products
		/// </summary>
		/// <returns>Products</returns>
		/// <response code="200">Products retrieved successfully</response>
		[HttpGet]
		[ProducesResponseType( typeof( IEnumerable<ProductReadmodel> ), 200 )]
		public async Task<ActionResult<IEnumerable<ProductReadmodel>>> GetAll()
		{
			var list = await _productService.GetAllAsync();

			return CreateResponse(list);
		}

		private ActionResult<TModel> CreateResponse<TModel>(TModel model)
		{
			var errors = new List<string>();
			errors.AddRange(_notifications.GetNotifications().Select(x => x.Value));

			if (!ModelState.IsValid)
			{
				var erros = ModelState.Values.SelectMany(v => v.Errors);
				foreach (var erro in erros)
				{
					var erroMsg = erro.Exception == null ? erro.ErrorMessage : erro.Exception.Message;
					errors.Add(erroMsg);
				}
			}

			if (!errors.Any())
			{
				return Ok(model);
			}
			else
			{
				return BadRequest(new
				{
					errors
				});
			}
		}
	}
}
