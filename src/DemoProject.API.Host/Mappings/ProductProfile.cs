﻿using AutoMapper;
using DemoProject.Application.Contracts;

namespace DemoProject.API.Host.Mappings
{
	public class ProductProfile : Profile
	{
		public ProductProfile()
		{
			CreateMap<Contracts.Product, Product>().ForMember(x=>x.Photo, opt=>opt.Ignore());
		}
	}
}
