﻿using System.Linq;
using DemoProject.API.Host.Controllers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DemoProject.API.Host.Extensions
{
	public class SwaggerFileOperation : IOperationFilter
	{
		public void Apply(Operation operation, OperationFilterContext context)
		{
			if (operation.OperationId == nameof(ProductsController.AssignPhoto) )
			{
				foreach (var opParam in operation.Parameters.ToArray())
				{
					if (opParam.Name.ToLower() != "id")
					operation.Parameters.Remove(opParam);
				}
				operation.Parameters.Add(new NonBodyParameter
				{
					Name = "photo",
					In = "formData",
					Description = "Upload Photo",
					Required = true,
					Type = "file"
				});
				operation.Consumes.Add("multipart/form-data");
			}
		}
	}

}
