﻿using AutoMapper;
using DemoProject.Application.Mappings;

namespace DemoProject.Application.Configuration
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProductProfile());
            });
        }
    }
}
