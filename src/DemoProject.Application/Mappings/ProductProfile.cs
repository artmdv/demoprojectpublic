﻿using AutoMapper;
using DemoProject.Application.Contracts;
using DemoProject.Domain.Commands;
using Product = DemoProject.Domain.Models.Product;

namespace DemoProject.Application.Mappings
{
	public class ProductProfile : Profile
	{
		public ProductProfile()
		{
			CreateMap<Product, ProductReadmodel>();
			CreateMap<ProductReadmodel, Contracts.Product>().ForMember(x => x.Photo, opt => opt.Ignore());

			CreateMap<Contracts.Product, AddProductCommand>()
				.ConstructUsing(c => new AddProductCommand(c.Code, c.Name, c.Price, c.Photo))
				.ForMember(x=>x.Timestamp, opt=>opt.Ignore())
				.ForMember(x=>x.ValidationResult, opt=>opt.Ignore());
			CreateMap<Contracts.Product, EditProductCommand>()
				.ConstructUsing(c => new EditProductCommand(c.Id, c.Code, c.Name, c.Price, c.Photo))
				.ForMember(x=>x.Timestamp, opt=>opt.Ignore())
				.ForMember(x=>x.ValidationResult, opt=>opt.Ignore());
		}
	}
}
