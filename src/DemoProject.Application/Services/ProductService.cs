﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DemoProject.Application.Contracts;
using DemoProject.Domain.Commands;
using DemoProject.Domain.Interfaces;
using MediatR;

namespace DemoProject.Application.Services
{
    public class ProductService: IProductService
    {
	    private readonly IMapper _mapper;
	    private readonly IMediator _mediator;
	    private readonly IProductRepository _productRepository;

	    public ProductService(IMapper mapper, IMediator mediator, IProductRepository productRepository)
	    {
		    _mapper = mapper;
		    _mediator = mediator;
		    _productRepository = productRepository;
	    }

        public async Task AddAsync(Product item)
        {
	        await _mediator.Send(_mapper.Map<AddProductCommand>(item));
        }

        public async Task EditAsync(Product item)
        {
	        await _mediator.Send(_mapper.Map<EditProductCommand>(item));
        }

        public async Task RemoveAsync(Guid id)
        {
	        await _mediator.Send(new RemoveProductCommand(id));
        }

        public async Task<ProductReadmodel> GetByIdAsync(Guid id)
        {
	        return _mapper.Map<ProductReadmodel>(await _productRepository.GetByIdAsync(id));
        }

        public Task<byte[]> ExportAsync()
        {
	        return _productRepository.Export();
        }

        public async Task<IEnumerable<ProductReadmodel>> GetAllAsync()
        {
	        var products = await _productRepository.GetAllAsync();
	        return products.ProjectTo<ProductReadmodel>(_mapper.ConfigurationProvider);
        }

        public async Task<IEnumerable<ProductReadmodel>> SearchAsync(string query)
        {
	        var products = await _productRepository.FindAsync(query);
	        return products.ProjectTo<ProductReadmodel>(_mapper.ConfigurationProvider);
        }

	    public async Task<byte[]> GetPhotoAsync(Guid id)
	    {
		    var product = await _productRepository.GetByIdAsync(id);
		    if (product.PhotoId != null)
		    {
			    return await _productRepository.GetPhotoAsync(product.PhotoId.Value);
		    }
		    return null;
	    }
    }
}
