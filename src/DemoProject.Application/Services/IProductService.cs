﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DemoProject.Application.Contracts;

namespace DemoProject.Application.Services
{
    public interface IProductService
    {
        Task AddAsync(Product item);
        Task EditAsync(Product item);
        Task RemoveAsync(Guid id);
        Task<ProductReadmodel> GetByIdAsync(Guid id);
        Task<byte[]> ExportAsync();
        Task<IEnumerable<ProductReadmodel>> GetAllAsync();
        Task<IEnumerable<ProductReadmodel>> SearchAsync(string query);
        Task<byte[]> GetPhotoAsync(Guid id);
    }
}
