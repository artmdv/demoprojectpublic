﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DemoProject.Application.Contracts
{
    //we want to have readmodel so we wouldnt return all the properties it has to the list (e.g. byte array of images)
    public class ProductReadmodel
    {
        [Key]
        public Guid Id { get; set; }
        
        [DisplayName("Code")]
        public string Code { get; set; }
        
        [DisplayName("Product")]
        public string Name { get; set; }
        
        [DisplayName("Price")]
        public decimal Price { get; set; }

        [DisplayFormat(DataFormatString = "{0:o}")]
        [DataType(DataType.DateTime)]
        [DisplayName("Last updated")]
        public DateTime LastUpdated { get; set; }
    }
}
