﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DemoProject.Application.Contracts
{
    public class Product
    {
        [Key]
        [ReadOnly(true)]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "The Code is Required")]
        [MaxLength(100)]
        [DisplayName("Code")]
        public string Code { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Product")]
        public string Name { get; set; }

        [DisplayName("Photo")]
        public byte[] Photo { get; set; }

        [Required]
        [Range(0, 999999999)]
        public decimal Price { get; set; }
    }
}
