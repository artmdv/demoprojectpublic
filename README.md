# DemoProjectPublic

Before running you need to update config files and run MS SQL database initialization 

DemoProject.UI.Host/appsettings.json - Update connection string to point to your database

DemoProject.API.Host/appsettings.json - Update connection string to point to your database

Run Update-Database against DemoProject.Infrastructure project to initialize database

------
TODO:
* finish unit tests
* finish integration tests
* improve API to return headers with content-location as well as created data in body
* create method in product service to update only image of a product
* save product photo filename so we could return image with correct extension
* think of best way to handle not found errors. and where to do that.
* metrics, logging
* maybe use some kind of transaction when checking if product with same code exists? 
* is the price confirmation when price > 999 supposed to be done in more explicit way, e.g. keeping domain entity in "unconfirmed" state (would need introducing states for product)

this project is just living here
